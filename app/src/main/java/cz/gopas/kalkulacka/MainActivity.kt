package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        /*
        if (savedInstanceState == null) {
//            supportFragmentManager.beginTransaction()
//                .replace(android.R.id.content, CalcFragment())
//                .commit()
            supportFragmentManager.commit {
                replace(android.R.id.content, CalcFragment())
            }
        }
         */

        Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Create")

        Log.d(TAG, "${intent.dataString}")

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navhost) as NavHostFragment
        val navController = navHostFragment.navController
        setupActionBarWithNavController(navController, AppBarConfiguration(navController.graph))
        findViewById<BottomNavigationView>(R.id.bottom_navigation).setupWithNavController(navController)
    }

    override fun onSupportNavigateUp() = findNavController(R.id.navhost).navigateUp() || super.onSupportNavigateUp()

    /*
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putCharSequence("res", result.text)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        result.text = savedInstanceState.getCharSequence("res")
    }
     */

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Stop")
    }

    private companion object {
        private val TAG = MainActivity::class.simpleName
    }
}