package cz.gopas.kalkulacka

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import androidx.room.Room
import cz.gopas.kalkulacka.history.HistoryDatabase
import cz.gopas.kalkulacka.history.HistoryEntity
import kotlin.concurrent.thread

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    //class CalcViewModel: ViewModel() {
    private val _result = MutableLiveData<Float>()
    val result: LiveData<Float> = _result

    private val prefs = PreferenceManager.getDefaultSharedPreferences(app)

    private val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
        //.allowMainThreadQueries()
        .build()
        .historyDao()

    var ans: Float
        get() = prefs.getFloat(ANS_KEY, 0f)
        private set(value) = prefs.edit { putFloat(ANS_KEY, value) }

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        thread {
            Thread.sleep(1000)
            val res = when (op) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.mul -> a * b
                R.id.div -> a / b
                else -> Float.NaN
            }
            ans = res
            db.add(HistoryEntity(res))
            _result.postValue(res)
        }
    }

    private companion object {
        const val ANS_KEY = "ans"
    }
}