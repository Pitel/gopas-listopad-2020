package cz.gopas.kalkulacka.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import kotlin.concurrent.thread

class HistoryViewModel(app: Application): AndroidViewModel(app) {
    val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
        //.allowMainThreadQueries()
        .build()
        .historyDao().apply {
            /*
            thread {
                (1..1000).forEach {
                    add(HistoryEntity(it.toFloat()))
                }
            }
             */
        }
}