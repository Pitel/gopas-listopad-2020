package cz.gopas.kalkulacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cz.gopas.kalkulacka.R

class HistoryFragment: Fragment(R.layout.fragment_history) {

    private val viewModel: HistoryViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recycler = view as RecyclerView
        val adapter = HistoryAdapter {
            with(findNavController()) {
                previousBackStackEntry?.savedStateHandle?.set(HISTORY_KEY, it)
                popBackStack()
            }
        }

        recycler.setHasFixedSize(true)
        recycler.adapter = adapter

        viewModel.db.getAll().observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
    }

    companion object {
        const val HISTORY_KEY = "history"
    }
}