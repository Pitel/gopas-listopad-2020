package cz.gopas.kalkulacka

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryFragment

class CalcFragment : Fragment() {

    private lateinit var binding: FragmentCalcBinding
    private val viewModel: CalcViewModel by viewModels()

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //return inflater.inflate(R.layout.activity_main, container, false)
        binding = FragmentCalcBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            ops.setOnCheckedChangeListener { _, _ -> calc() }
            calc.setOnClickListener { calc() }
            share.setOnClickListener { share() }
            viewModel.result.observe(viewLifecycleOwner) {
                result.text = "$it"
            }
            ans.setOnClickListener {
                b.editText?.setText(viewModel.ans.toString())
            }
            history.setOnClickListener {
                findNavController().navigate(R.id.action_history)
            }
            findNavController().currentBackStackEntry?.savedStateHandle
                ?.getLiveData<Float>(HistoryFragment.HISTORY_KEY)
                ?.observe(viewLifecycleOwner) {
                    a.editText?.setText("$it")
                }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            Log.d(TAG, "About")
            findNavController().navigate(R.id.action_about)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun calc() {
        Log.d(TAG, "Calc!")
        val a: Float = binding.a.editText?.text?.toString()?.toFloatOrNull() ?: 0f
        val b: Float = binding.b.editText?.text?.toString()?.toFloatOrNull() ?: 0f
        if (binding.ops.checkedRadioButtonId == R.id.div && b == 0f) {
            findNavController().navigate(R.id.action_zero)
        } else {
            viewModel.calc(a, b, binding.ops.checkedRadioButtonId)
        }
    }

    private fun share() {
        Log.d(TAG, "Share")
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, getString(R.string.share_str, binding.result.text))
        }
        startActivity(intent)
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}